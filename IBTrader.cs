﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using Ecng.Common;
using Ecng.Interop;
using StockSharp.Algo;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using TWSLib;

namespace IBTraderActiveX
{
    public class IBTrader : BaseTrader
    {
        private const string _contractId = "contractId";
        private  int _clientId;
        private readonly EntityRegistry _entityRegistry;
        private   OrderList _orderList;
        private TradeList _tradeList;
        private MyTradeList _myTradeList;
        private bool _firstLaunch = true;
       
        private  string _host = "";
        private readonly TwsClass _ib = new TwsClass();
        private  int _port;

        private readonly Dictionary<int, Security> _securityDetailsDictionary = new Dictionary<int, Security>();
        private readonly Dictionary<int, CandleSeries> _candleseriesHistoricalDataDictionary = new Dictionary<int, CandleSeries>();
       // private readonly Dictionary<int, TimeSpan> _timeframeHistoricalDataDictionary = new Dictionary<int, TimeSpan>();

        private readonly Dictionary<int, Security> _securityMarketDataDictionary = new Dictionary<int, Security>();
        private readonly Dictionary<int, Security> _securityMarketDepthictionary = new Dictionary<int, Security>();
        private readonly Dictionary<int, Security> _securityRealTimeCandlesDictionary = new Dictionary<int, Security>();

        private string _displayName;
        private int _nextOrderId;
        private int _uniqueId;

        public event Action<TimeFrameCandle> NewRealTimeCandle;
        public event Action<CandleSeries, IEnumerable<TimeFrameCandle>> NewHistoricalCandles;

        public IBTrader(EntityRegistry entityRegistry, string host = "127.0.0.1", int port = 7496, int id = 0)
            : base(Platforms.AnyCPU, false)
        {
            Host = host;
            Port = port;
            ClientId = id;
            _entityRegistry = entityRegistry;
            _orderList = new OrderList(_entityRegistry.Storage);
            _myTradeList = new MyTradeList(_entityRegistry.Storage);
            _tradeList = new TradeList(_entityRegistry.Storage);

            _ib.nextValidId += OnNextValidId;
            _ib.connectionClosed += OnConnectionClosed;
            _ib.updateAccountValue += OnUpdateAccountValue;
            _ib.updatePortfolioEx += OnUpdatePortfolio;
            _ib.tickPrice += OnTickPrice;
            _ib.tickSize += OnTickSize;
            _ib.tickSnapshotEnd += OnTickSnapshotEnd;
            _ib.openOrderEx += OnOpenOrder;
            _ib.orderStatus += OnOrderStatus;
            _ib.contractDetailsEx += OnContractDetails;

            _ib.accountDownloadEnd += OnAccountDownloadEnd;
            _ib.errMsg += OnError;
            _ib.execDetailsEx += OnExecDetails;
            _ib.currentTime += OnCurrentTime;
            _ib.realtimeBar += OnRealTimeBar;
            _ib.historicalData += OnHistoricalData;

           // NewOrders += SaveOrders;
          //  OrdersChanged += SaveOrders;


            TransactionIdGenerator = new IncrementTransactionIdGenerator();
            ((IncrementTransactionIdGenerator)TransactionIdGenerator).CurrentTransactionId = 0;
        }



       

        public override string DisplayName
        {
            get { return "IBTrader"; }
        }

        public int ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }


        public static int? GetContractId(Security security)
        {
            return security.GetValue<int?>(_contractId);
        }


        public static void SetContractId(Security security, int contractId)
        {
            security.AddValue(_contractId, contractId);
        }


        private int GetUniqueId()
        {
            _uniqueId++;
            return _uniqueId;
        }

        private string GetSecurityId(string secCode, ExchangeBoard board)
        {
            return SecurityIdGenerator.GenerateId(secCode,  board);
        }

        private Security GetSecurity(string secCode, ExchangeBoard board, Action<Security> handler)
        {
            if (handler == null)
                throw new ArgumentNullException("handler");

            return GetSecurity(GetSecurityId(secCode, board), id =>
            {
                Security security =
                    EntityFactory.CreateSecurity(id);
                handler(security);
                return security;
            }, s => false);
        }


        private void TryAddNativeSecurityId(Security security, int nativeId)
        {
            if (nativeId == -1)
                return;

            if (GetContractId(security) != null)
                return;

            SetContractId(security, nativeId);
            AddNativeSecurityId(security, GetSecurityId(nativeId.To<string>(), security.ExchangeBoard));
        }

        private IOrder GetOrder(Order order)
        {
            IOrder ibOrder = _ib.createOrder();

            ibOrder.transmit = 1;

            var tif = order.GetValue<string>("timeInForce");
            if (!string.IsNullOrEmpty(tif)) ibOrder.timeInForce = tif;
            else ibOrder.timeInForce = "GTC";
            ibOrder.outsideRth = 0;


            ibOrder.orderId = (int)order.TransactionId;
            switch (order.Direction)
            {
                case OrderDirections.Sell:
                    ibOrder.action = "SELL";
                    break;

                case OrderDirections.Buy:
                    ibOrder.action = "BUY";
                    break;
                default:
                    throw new InvalidOperationException("Неизвестное направление заявки.");
            }

            ibOrder.totalQuantity = (int)order.Volume;

            switch (order.Type)
            {
                case OrderTypes.Market:
                    ibOrder.orderType = "MKT";


                    break;

                case OrderTypes.Limit:
                    ibOrder.orderType = "LMT";
                    ibOrder.lmtPrice = (double)order.Price;
                    break;
                case OrderTypes.Conditional:
                    var extendedType = order.GetValue<string>("extendedType");
                    if (string.IsNullOrEmpty(extendedType)) throw new ArgumentException("Неизвестный тип заявки.");

                    ibOrder.orderType = extendedType;

                    ibOrder.auxPrice = (double)order.Price;
                    break;

                default:
                    throw new ArgumentException("Неизвестный тип заявки.");
            }


            return ibOrder;
        }

        private IContract GetContract(Security security)
        {
            IContract contract = _ib.createContract();
            // var id = security.GetValue<int>("contractId");;
            // if (id != null)
            //    contract.conId = id;


            switch (security.Type)
            {
                case SecurityTypes.Stock:
                    contract.secType = "STK";

                    break;

                case SecurityTypes.Future:
                    contract.secType = "FUT";
                    var ExpirationString = new StringBuilder();
                    ExpirationString.AppendFormat("{0:0000}{1:00}", security.ExpiryDate.Value.Year,
                                                  security.ExpiryDate.Value.Month);
                    contract.expiry = ExpirationString.ToString();

                    break;

                case SecurityTypes.Index:
                    contract.secType = "IND";
                    break;
                default:
                    throw new ArgumentException("Тип инструмента не поддерживается.");
            }

            contract.exchange = security.ExchangeBoard.Code;
            contract.symbol = security.Code;
            switch (security.Currency)
            {
                case CurrencyTypes.USD:
                    contract.currency = "USD";

                    break;
            }


            return contract;
        }

        private Security GetSecurity(IContract contract)
        {
            /*   if (Securities.All(s => s.Code != contract.symbol))
             {
                 var security = EntityFactory.CreateSecurity(contract.symbol + "@Smart");
                 LookupSecurities(security);
                 return security;
             }
            return Securities.First(s => s.Code == contract.symbol);
            */

            int nativeId = contract.conId;
            string secCode = contract.symbol;
            SecurityTypes type;
            switch (contract.secType)
            {
                case "STK":
                    type = SecurityTypes.Stock;
                    break;
                case "FUT":
                    type = SecurityTypes.Future;
                    break;
                case "IND":
                    type = SecurityTypes.Index;
                    break;

                default:
                    type = SecurityTypes.Stock;
                    this.AddErrorLog("Неизвестный тип инструмента {0}".Put(contract.secType));

                    break;
            }

            DateTime? expiryDate = null;

            if (!String.IsNullOrEmpty(contract.expiry))
            {
                expiryDate = DateTime.ParseExact(contract.expiry, "yyyyMMdd", CultureInfo.InvariantCulture);
            }

            var strike = (decimal)contract.strike;

            OptionTypes? optionType = null;

            if (contract.right.Equals("C") || contract.right.Equals("CALL")) optionType = OptionTypes.Call;
            else if (contract.right.Equals("P") || contract.right.Equals("PUT")) optionType = OptionTypes.Call;
            decimal? multiplier = null;
            if (!String.IsNullOrEmpty(contract.multiplier)) multiplier = contract.multiplier.To<decimal>();
            ExchangeBoard exchangeBoard = null;
            string boardCode = "";
            if (type == SecurityTypes.Stock)
            {
                if (contract.primaryExchange == "NYSE")
                {
                    exchangeBoard = ExchangeBoard.Nyse;
                }
                else
                {
                    exchangeBoard = ExchangeBoard.Nasdaq;
                }

                exchangeBoard.Code = "SMART";
            }
            else
            {
                

                if (!String.IsNullOrEmpty(contract.primaryExchange)) boardCode = contract.primaryExchange;
                else boardCode = contract.exchange;


                exchangeBoard = ExchangeBoard.GetOrCreateBoard(boardCode, code => new ExchangeBoard
                                                                              {
                                                                                  Exchange = new Exchange {Name = code},
                                                                                  Code = code,
                                                                                  IsSupportAtomicReRegister = true,
                                                                                  IsSupportMarketOrders = true, 
                                                                                  WorkingTime = ExchangeBoard.Nasdaq.WorkingTime.Clone() 
                                                                              });
            }

            var currency = contract.currency.To<CurrencyTypes>();
            if (secCode.IsEmpty())
                throw new ArgumentNullException("secCode");
            if (!String.IsNullOrEmpty(contract.localSymbol)) secCode = contract.localSymbol;


            Security security = GetSecurity(secCode, exchangeBoard, s =>
            {
                s.Name = secCode;
                s.Code = secCode;
                s.Type = type;
                s.ExpiryDate = expiryDate;
                s.Strike = strike;
                s.OptionType = optionType;
                s.Currency = currency;
                s.ExchangeBoard = exchangeBoard;
                s.MinStepSize = 0.01m;
                s.MinStepPrice = 0.01m;

                if (multiplier != null)
                    s.AddValue("Multiplier", multiplier);
            });
            TryAddNativeSecurityId(security, nativeId);
            SetRequestIdLists(security);

            return security;
        }

        private Order GetOrder(IOrder ibOrder, Security security, IOrderState ibOrderState)
        {
            int transactionId = ibOrder.orderId;
            Order order = GetOrderByTransactionId(transactionId);

            if (order == null)
            {
                order = EntityFactory.CreateOrder(security, transactionId);
                order.TransactionId = transactionId;
            }

            order = GetOrder(security, transactionId, id =>
            {
                using (order.BeginUpdate())
                {
                    order.Id = id;

                    if (ibOrder.action.Equals("SELL"))
                        order.Direction = OrderDirections.Sell;
                    else if (ibOrder.action.Equals("BUY"))
                        order.Direction = OrderDirections.Buy;

                    OrderTypes type;
                    string extendedType;

                    switch (ibOrder.orderType)
                    {
                        case "LMT":
                            type = OrderTypes.Limit;
                            extendedType = "";
                            break;
                        case "MKT":
                            type = OrderTypes.Market;
                            extendedType = "";
                            break;
                        default:
                            type = OrderTypes.Conditional;
                            extendedType = ibOrder.orderType;
                            break;
                    }

                    order.Type = type;
                    order.Volume = ibOrder.totalQuantity;
                    order.AddValue("extendedType", extendedType);
                    if (order.Type == OrderTypes.Limit)
                        order.Price = (decimal)ibOrder.lmtPrice;
                    else if (order.Type != OrderTypes.Market)
                        order.Price = (decimal)ibOrder.auxPrice;

                    if (!String.IsNullOrEmpty(ibOrder.goodTillDate))
                        order.ExpiryDate =
                            DateTime.ParseExact(ibOrder.goodTillDate,
                                                "YYYYMMDD hh:mm:ss",
                                                CultureInfo.
                                                    InvariantCulture);

                    else order.ExpiryDate = DateTime.MaxValue;


                    order.Portfolio = GetPortfolio(ibOrder.account);

                    order.AddValue("clientId", ibOrder.clientId);
                    order.AddValue("permId", ibOrder.permId);

                    order.AddValue("timeInForce", ibOrder.timeInForce);
                    if (ibOrder.timeInForce.Equals("GTC"))
                        order.ExpiryDate = DateTime.MaxValue;

                    OrderStatus status;

                    switch (ibOrderState.status)
                    {
                        case "ApiCancelled":
                            order.Status = OrderStatus.GateError;
                            order.State = OrderStates.Failed;

                            break;

                        case "ApiPending":
                            break;


                        case "Canceled":
                            order.State = OrderStates.Failed;
                            order.Status = OrderStatus.NotDone;
                            break;


                        case "Error":
                            order.State = OrderStates.Failed;

                            break;

                        case "Filled":
                            order.State = OrderStates.Done;
                            order.Status = OrderStatus.Matched;
                            order.Balance = 0m;

                            break;

                        case "Inactive":
                            order.State = OrderStates.Failed;
                            order.Status = OrderStatus.NotValidated;
                            break;

                        case "None":
                            order.State = OrderStates.None;
                           


                            break;

                        case "PartiallyFilled":
                            //  order.Balance = ibOrderState.remaining;

                            break;
                        case "PendingCancel":

                            break;

                        case "PendingSubmit":
                            order.Status = OrderStatus.SentToServer;
                            break;
                        case "PreSubmitted":
                            order.Status = OrderStatus.SentToServer;
                            order.State = OrderStates.Active;
                            break;
                        case "Submitted":
                            order.Status = OrderStatus.Accepted;
                            order.State = OrderStates.Active;

                            break;
                    }
                }

                return order;
            }, o => false);


            return order;
        }
        
        private void SetRequestIdLists(Security security)
        {
           
            if (security.ExtensionInfo == null) security.ExtensionInfo = new Dictionary<object, object>();

            if (!security.ExtensionInfo.ContainsKey("detailsReqId"))
            {
               
             
                security.AddValue("detailsReqId", new List<int>());
            }
            //  _ib.reqContractDetailsEx(requestId,contract);
            if (!security.ExtensionInfo.ContainsKey("marketDataReqId"))
            {
                security.AddValue("marketDataReqId", new List<int>());
            }

            if (!security.ExtensionInfo.ContainsKey("realTimeCandlesReqId"))
            {
                security.AddValue("realTimeCandlesReqId", new List<int>());
            }

            if (!security.ExtensionInfo.ContainsKey("historicalDataReqId"))
            {

                security.AddValue("historicalDataReqId", new List<int>());
            }

            if (!security.ExtensionInfo.ContainsKey("marketDepthReqId"))
            {
                security.AddValue("marketDepthReqId", new List<int>());
            }
        }
        
        public new void LookupSecurities(Security security)
        {
            if (Securities.All(s => s.Code != security.Code))
            {
                //  security.Id = security.Code + "@" + security.ExchangeBoard.Code;

                if (security.Id == null)
                    security.Id = SecurityIdGenerator.GenerateId(security.Code,  security.ExchangeBoard);
                else
                {
                    if (security.Id.Count() == 0) security.Id = SecurityIdGenerator.GenerateId(security.Code,  security.ExchangeBoard);

                }

                SetRequestIdLists(security);


                if (security.BestAsk == null) security.BestAsk = new Quote();
                if (security.BestBid == null) security.BestBid = new Quote();

                //  _ib.reqMktDataEx(requestId,contract,"",1);

                RaiseNewSecurities(new[] { security });
            }
        }

        private string ConvertTimeFrame(TimeSpan timeFrame)
        {
         
            string barSize;
            if (timeFrame.TotalSeconds == TimeSpan.FromDays(1).TotalSeconds)
                barSize = "1 day";
            else
                if (timeFrame.TotalSeconds == TimeSpan.FromHours(1).TotalSeconds)
                    barSize = "1 hour";
                else
                    if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(30).TotalSeconds)
                        barSize = "30 mins";
                    else
                        if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(15).TotalSeconds)
                            barSize = "15 mins";
                        else
                            if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(5).TotalSeconds)
                                barSize = "5 mins";
                            else
                                if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(3).TotalSeconds)
                                    barSize = "3 mins";
                                else
                                    if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(2).TotalSeconds)
                                        barSize = "2 mins";
                                    else
                                        if (timeFrame.TotalSeconds == TimeSpan.FromMinutes(1).TotalSeconds)
                                            barSize = "1 min";
                                        else
                                            if (timeFrame.TotalSeconds == TimeSpan.FromSeconds(30).TotalSeconds)
                                                barSize = "30 secs";
                                            else
                                                if (timeFrame.TotalSeconds == TimeSpan.FromSeconds(15).TotalSeconds)
                                                    barSize = "15 secs";
                                                else
                                                    if (timeFrame.TotalSeconds == TimeSpan.FromSeconds(5).TotalSeconds)
                                                        barSize = "5 secs";
                                                    else
                                                        if (timeFrame.TotalSeconds == TimeSpan.FromSeconds(1).Seconds)
                                                            barSize = "1 sec";
                                                        else throw new ArgumentException("TimeFrame для исторических данных не поддерживается.");
            return barSize;
        }


        private TimeSpan ConvertTimeFrame(string barSize)
        {
            TimeSpan timeFrame;

            switch (barSize)
            {
                case "1 day":
                    return TimeSpan.FromDays(1);
                    break;
                case "1 hour":
                    return TimeSpan.FromHours(1);
                    break;
                case "30 mins":
                    return TimeSpan.FromMinutes(30);
                    break;
                case "15 mins":
                    return TimeSpan.FromMinutes(15);
                    break;
                case "5 mins":
                    return TimeSpan.FromMinutes(5);
                case "3 mins":
                    return TimeSpan.FromMinutes(3);
                    break;
                case "2 mins":
                    return TimeSpan.FromMinutes(2);

                    break;
                case "1 min":
                    return TimeSpan.FromMinutes(1);
                    break;
                case "30 secs":
                    return TimeSpan.FromSeconds(30);
                    break;
                case "15 secs":
                    return TimeSpan.FromSeconds(15);
                    break;
                case "5 secs":
                    return TimeSpan.FromSeconds(5);
                    break;
                case "1 sec":
                    return TimeSpan.FromSeconds(1);
                    break;

                default:
                    throw new ArgumentException("BarSize для исторических данных не поддерживается" + barSize);



            }
        }

        protected override void OnConnect()
        {
            this.AddInfoLog("Производим подключение...");
            Process[] processes = Process.GetProcesses();
            bool isTWSstarted = false;
            foreach (Process item in processes)
            {
                if (item.ProcessName == "javaw") isTWSstarted = true;
            }

            if (!isTWSstarted) this.AddErrorLog("TWS не запущен.");
            else
            {
                _ib.connect(Host, Port, ClientId);


                //    RaiseConnected();
                // IsConnected = true;
                _ib.reqCurrentTime();
            }
        }


        private void OnRealTimeBar(int tickerid, int time, double open, double high, double low, double close,
                                   int volume, double wap, int count)
        {

            if (!_securityRealTimeCandlesDictionary.ContainsKey(tickerid))
            {
                this.AddErrorLog("RealTimeCandlesRequestId не найден.  ReqId= {0} " .Put( tickerid));
                _ib.cancelRealTimeBars(tickerid);
                return;
            }

            Security security = _securityRealTimeCandlesDictionary[tickerid];
            var candle = new TimeFrameCandle();
            candle.OpenPrice = (decimal)open;
            candle.HighPrice = (decimal)high;
            candle.LowPrice = (decimal)low;
            candle.ClosePrice = (decimal)close;
            candle.TotalVolume = volume;
            candle.OpenTime = Converter.GregorianStart.AddSeconds(time);
            candle.Security = security;
            candle.TimeFrame = TimeSpan.FromSeconds(5);
            candle.TotalPrice = (decimal)wap;
            candle.CloseVolume = count;

            candle.CloseTime = candle.OpenTime + candle.TimeFrame;
            candle.State = CandleStates.Finished;

            NewRealTimeCandle.SafeInvoke(candle);

        }

        private void OnHistoricalData(int reqid, string date, double open, double high, double low, double close, int volume, int barcount, double wap, int hasgaps)
        {

            if (!_candleseriesHistoricalDataDictionary.ContainsKey(reqid))
            {
                this.AddErrorLog("HistoricalDataRequestId не найден.  ReqId= {0} " .Put( reqid));
                _ib.cancelHistoricalData(reqid);
                return;
            }
            var series = _candleseriesHistoricalDataDictionary[reqid];
            var security = series.Security;

            var candle = new TimeFrameCandle();
            candle.OpenPrice = (decimal)open;
            candle.HighPrice = (decimal)high;
            candle.LowPrice = (decimal)low;
            candle.ClosePrice = (decimal)close;
            candle.TotalVolume = volume;
            if (date.Length > 8) candle.OpenTime = DateTime.ParseExact(date, "yyyyMMdd  HH:mm:ss", CultureInfo.InvariantCulture);
            else candle.OpenTime = DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture);
            candle.Security = security;
            candle.TimeFrame = (TimeSpan)series.Arg;
            candle.TotalPrice = (decimal)wap;
            candle.CloseVolume = barcount;


            candle.CloseTime = candle.OpenTime + candle.TimeFrame;
            candle.State = CandleStates.Finished;
            
            NewHistoricalCandles.SafeInvoke(series,new []{candle});

        }

        private void SaveMyTrades()
        {
            foreach (var myTrade in MyTrades)
            {
                _entityRegistry.MyTrades.Save(myTrade);
            }
        }

        private void SaveOrders()
        {
            foreach (Order order in Orders)
            {
                _entityRegistry.Orders.Save(order);
            }
        }


        private void OnUpdateAccountValue(string key, string value, string currency, string name)
        {
            Portfolio portfolio = GetPortfolio(name);
            portfolio.AddValue(key, value);
        }


        protected override void OnDisconnect()
        {
            this.AddInfoLog("Производим отключение...");
            if (!IsConnected) RaiseDisconnected();
            else
            {
                _ib.disconnect();
                base.OnDisconnect();
            }
        }

        protected override void OnRegisterOrder(Order order)
        {
            IOrder ibOrder = GetOrder(order);
            IContract contract = GetContract(order.Security);
            ibOrder.orderId = (int)order.TransactionId;

            _ib.placeOrderEx(ibOrder.orderId, contract, ibOrder);
            IExecutionFilter filter = _ib.createExecutionFilter();
            filter.clientId = ClientId;
            filter.symbol = contract.symbol;
            _ib.reqExecutionsEx(ibOrder.orderId, filter);
        }

        protected override void OnReRegisterOrder(Order oldOrder, Order newOrder)
        {
            IOrder newIbOrder = GetOrder(newOrder);

            IContract contract = GetContract(newOrder.Security);

            _ib.placeOrderEx((int)oldOrder.TransactionId, contract, newIbOrder);
            IExecutionFilter filter = _ib.createExecutionFilter();
            filter.clientId = ClientId;
            filter.symbol = contract.symbol;
            _ib.reqExecutionsEx(newIbOrder.orderId, filter);
        }


        protected override void OnCancelOrder(Order order)
        {
            _ib.cancelOrder((int)order.TransactionId);
        }

        public void RegisterSecuritySnapshot(Security security)
        {

           var requestId = GetUniqueId();
            _securityMarketDataDictionary.Add(requestId, security);
            ((List<int>)security.ExtensionInfo["marketDataReqId"]).Add(requestId);
          
            IContract contract = GetContract(security);
            _ib.reqMktDataEx(requestId, contract, "", 1);

        }

        protected override void OnRegisterSecurity(Security security)
        {
            var requestId = GetUniqueId();
            _securityMarketDataDictionary.Add(requestId, security);
            ((List<int>)security.ExtensionInfo["marketDataReqId"]).Add(requestId);
            IContract contract = GetContract(security);
            _ib.reqMktDataEx(requestId, contract, "", 0);
        }

        protected override void OnRegisterTrades(Security security)
        {
            base.OnRegisterTrades(security);
            OnRegisterSecurity(security);
        }

        protected override void OnUnRegisterTrades(Security security)
        {
            base.OnUnRegisterTrades(security);

            foreach (var key in ((List<int>)security.ExtensionInfo["marketDataReqId"]))
            {
                
                    _ib.cancelMktData(key);
            }
        }

        protected override void OnUnRegisterSecurity(Security security)
        {
            base.OnUnRegisterSecurity(security);

            foreach (var key in ((List<int>)security.ExtensionInfo["marketDataReqId"]))
            {
               
                    _ib.cancelMktData(key);
            }
           
          
        }

        protected override void OnRegisterPortfolio(Portfolio portfolio)
        {
            base.OnRegisterPortfolio(portfolio);
            if (portfolio == null) throw new ArgumentNullException("portfolio");
            _ib.reqAccountUpdates(1, "");
        }

        protected override void OnUnRegisterPortfolio(Portfolio portfolio)
        {
            base.OnUnRegisterPortfolio(portfolio);
            if (portfolio == null) throw new ArgumentNullException("portfolio");
            _ib.reqAccountUpdates(0, portfolio.Name);
        }

        public void SubscribeRealTimeCandles(Security security, TimeSpan timeFrame,
                                             SecurityChangeTypes dataType = SecurityChangeTypes.LastTrade,
                                             bool useRth = true)
        {
            IContract contract = GetContract(security);
           var requestId = GetUniqueId();
           ((List<int>)security.ExtensionInfo["realTimeCandlesReqId"]).Add(requestId);
          
            
            _securityRealTimeCandlesDictionary.Add(requestId, security); 
            int barSize;
            if (timeFrame.Seconds == 5) barSize = 5;
            else throw new ArgumentException("Таймфрейм для RealTimeCandles не поддерживается.");

            string type;
            switch (dataType)
            {
                case SecurityChangeTypes.LastTrade:
                    type = "TRADES";
                    break;

                case SecurityChangeTypes.BestBid:
                    type = "BID";
                    break;
                case SecurityChangeTypes.BestAsk:
                    type = "ASK";
                    break;

                default:
                    throw new ArgumentException("SecurityChangeTypes для RealTimeCandles не поддерживается.");
                    break;
            }
            int rth;
            if (useRth) rth = 1;
            else rth = 0;


            _ib.reqRealTimeBarsEx(requestId, contract, barSize, type, rth);
        }

        public void UnSubscribeRealTimeCandles(Security security)
        {


            foreach (var key in ((List<int>)security.ExtensionInfo["realTimeCandlesReqId"]))
            {
             
                    _ib.cancelRealTimeBars(key);
            }
        }

        private static string ConvertToHistoryReqPeriod(DateTime startTime, DateTime endTime)
        {
            var period = endTime.Subtract(startTime);
            var secs = period.TotalSeconds;
            long unit;

            if (secs < 1)
                throw new ArgumentOutOfRangeException("endTime", "Period cannot be less than 1 second.");

            if (secs < 86400)
            {
                unit = (long)Math.Ceiling(secs);
                return unit + " S";
            }

            var days = secs / 86400;

            unit = (long)Math.Ceiling(days);

            if (unit <= 34)
                return unit + " D";

            var weeks = days / 7;
            unit = (long)Math.Ceiling(weeks);

            if (unit > 52)
                throw new ArgumentOutOfRangeException("endTime", "Period cannot be bigger than 52 weeks.");

            return unit + " W";
        }
     //   public void SubscribeHistoricalCandles(Security security, DateTime startDate, DateTime endDate,
      //                               TimeSpan timeFrame, SecurityChangeTypes dataType = SecurityChangeTypes.LastTrade, bool useRth = true)
      
      public void SubscribeHistoricalCandles(CandleSeries series, DateTime from, DateTime to)
    
    {
        var contract = GetContract(series.Security);
        var requestId = GetUniqueId();
            ((List<int>)series.Security.ExtensionInfo["historicalDataReqId"]).Add(requestId);
          
          
        _candleseriesHistoricalDataDictionary.Add(requestId, series);
        // string barSize;
           

        string type = "TRADES";
          /*
            switch (dataType)
            {
                case SecurityChangeTypes.LastTrade:
                    type = "TRADES";
                    break;

                case SecurityChangeTypes.BestBid:
                    type = "BID";
                    break;
                case SecurityChangeTypes.BestAsk:
                    type = "ASK";
                    break;
                case SecurityChangeTypes.ImpliedVolatility:
                    type = "OPTION_IMPLIED_VOLATILITY";
                    break;
                case SecurityChangeTypes.HistoricalVolatility:
                    type = "HISTORICAL_VOLATILITY";
                    break;

                default:
                    throw new ArgumentException("SecurityChangeTypes для RealTimeCandles не поддерживается.");
                    break;
            }*/
            int rth=1;
          //  if (useRth) rth = 1;
         //   else rth = 0;

            var barSize = ConvertTimeFrame((TimeSpan)series.Arg);

           

            var reqEndDate = to.ToUniversalTime().ToString("yyyyMMdd HH:mm:ss") + " GMT";
            var duration = ConvertToHistoryReqPeriod(from, to);

            _ib.reqHistoricalDataEx(requestId, contract, reqEndDate, duration, barSize, type, rth, 1);

        }

      public   void UnSubscribeHistoricalCandles(CandleSeries series)
      {

          foreach (var key in ((List<int>)series.Security.ExtensionInfo["historicalDataReqId"]))
          {
              var currSeries = _candleseriesHistoricalDataDictionary[key];
              if (currSeries.Security.Code == series.Security.Code && currSeries.From==series.From
                  && currSeries.To==series.To && (TimeSpan) currSeries.Arg==(TimeSpan) series.Arg)
                  _ib.cancelHistoricalData(key);
          }
      }

        public override void StartExport()
        {
            if (IsExportStarted)
            {
                this.AddWarningLog("Экспорт уже выполняется");
                return;
            }

            base.StartExport();


            _ib.reqIds(1);
            IExecutionFilter filter = _ib.createExecutionFilter();
            filter.clientId = ClientId;
            filter.time = (CurrentTime - TimeSpan.FromDays(14)).ToString("yyyyMMdd-hh:mm:ss");
            _ib.reqExecutionsEx(0, filter);
            _ib.reqOpenOrders();
            _ib.reqAllOpenOrders();
            _ib.reqAutoOpenOrders(1);
            switch (LogLevel)
            {
                case LogLevels.Info:
                    _ib.setServerLogLevel(4);
                    break;
                case LogLevels.Debug:
                    _ib.setServerLogLevel(1);
                    break;
                case LogLevels.Error:
                    _ib.setServerLogLevel(2);
                    break;
                case LogLevels.Warning:
                    _ib.setServerLogLevel(3);
                    break;
                case LogLevels.Off:
                    _ib.errMsg -= OnError;
                    // _ib.setServerLogLevel(Krs.Ats.IBNet.LogLevel.Undefined);
                    break;
                default :
                    _ib.setServerLogLevel(4);
                    break;
            }

             if(_firstLaunch)
             {
                 _firstLaunch = false;
                 LoadState();

             }
        }

        private void LoadState()
        {
              
            foreach (var readOrder in _orderList)
            {


                if (readOrder != null)
                {
                    var order = EntityFactory.CreateOrder(readOrder.Security, readOrder.TransactionId);
                    order.TransactionId = readOrder.TransactionId;
                    //

                    order = GetOrder(readOrder.Security, readOrder.TransactionId, id =>
                                                                                      {
                                                                                          using (order.BeginUpdate())
                                                                                          {
                                                                                              order.Id = id;
                                                                                              order.Direction =
                                                                                                  readOrder.Direction;
                                                                                              order.Type =
                                                                                                  readOrder.Type;
                                                                                              order.Volume =
                                                                                                  readOrder.Volume;
                                                                                              order.Price =
                                                                                                  readOrder.Price;
                                                                                              order.ExpiryDate =
                                                                                                  readOrder.ExpiryDate;
                                                                                              order.Portfolio =
                                                                                                  readOrder.Portfolio;
                                                                                              order.State =
                                                                                                  readOrder.State;
                                                                                              order.Status =
                                                                                                  readOrder.Status;
                                                                                              order.Time =
                                                                                                  readOrder.Time;
                                                                                              order.LatencyRegistration
                                                                                                  =
                                                                                                  readOrder.
                                                                                                      LatencyRegistration;
                                                                                              order.LatencyCancellation
                                                                                                  =
                                                                                                  readOrder.
                                                                                                      LatencyCancellation;
                                                                                              order.LastChangeTime =
                                                                                                  readOrder.
                                                                                                      LastChangeTime;
                                                                                          }

                                                                                          return order;
                                                                                      }, o => false);
                }
            }

            foreach (var myTrade in _myTradeList)
            {

                AddMyTrade(myTrade.Trade.Security, myTrade.Order.TransactionId, 0, myTrade.Trade.Id, id =>
                {
                    Trade trade =
                        EntityFactory.CreateTrade(myTrade.Trade.Security, id);

                    trade.Time = myTrade.Trade.Time;
                    trade.OrderDirection = myTrade.Trade.OrderDirection;
                    trade.Volume = myTrade.Trade.Volume;
                    trade.Price = myTrade.Trade.Price;
                    
                    return trade;
                }, mt =>
                {
                 //watafak

               
                });


            }
        }

        public override void StopExport()
        {
            SaveState();
              
             base.StopExport();
            
         }

        private void SaveState()
        {
            foreach (var order in Orders)
            {
                _orderList.Save(order);
            }

            foreach (var trade in Trades)
            {
                _tradeList.Save(trade);
            }
            foreach (var myTrade in MyTrades)
            {
                _myTradeList.Save(myTrade);
            }

           
        }

        private void OnCurrentTime(int time)
        {
            this.AddInfoLog("Подключение было произведено успешно.");
            IsConnected = true;
        }

        private void OnError(int id, int code, string msg)
        {
            if (id >= 2100 && id <= 2110)
            {
                this.AddWarningLog(msg);
                return;
            }
            if ((id >= 1100 && id <= 1102) || id == 1300 || id == -1)
            {
                this.AddInfoLog(msg);
                return;
            }

            this.AddErrorLog(msg);
        }

        private void OnOrderStatus(int id, string status, int filled, int remaining, double avgFillPrice, int permId,
                                   int parentId, double lastFillPrice, int clientId, string whyHeld)
        {
            var orderFail = new OrderFail();

            Order order = GetOrderByTransactionId(id);

            this.AddDebugLog("OnOrderStatus. status= {0} id= {1}".Put(status ,id));
            if (order != null)
            {
                switch (status)
                {
                    case "ApiCancelled":
                        order.Status = OrderStatus.GateError;
                        order.State = OrderStates.Failed;
                        this.AddOrderErrorLog(order, "IB OrderStatus: Api cancelled");
                        orderFail.Order = order;
                        orderFail.Error = new SystemException("Api cancelled");
                        RaiseOrdersRegisterFailed(new[] { orderFail });
                        break;

                    case "ApiPending":
                        break;


                    case "Canceled":
                        order.State = OrderStates.Done;
                        order.Status = OrderStatus.Cancelled;


                        break;


                    case "Error":
                        order.State = OrderStates.Failed;
                        order.Status = OrderStatus.NotDone;
                        orderFail.Order = order;
                        orderFail.Error = new SystemException("Api error");
                        RaiseOrdersRegisterFailed(new[] { orderFail });
                        break;

                    case "Filled":
                        order.State = OrderStates.Done;
                        order.Status = OrderStatus.Matched;
                        order.Balance = 0m;

                        break;

                    case "Inactive":
                        order.State = OrderStates.Failed;
                        order.Status = OrderStatus.NotValidated;
                        orderFail.Order = order;
                        orderFail.Error = new SystemException("Inactive");
                        RaiseOrdersRegisterFailed(new[] { orderFail });
                        this.AddOrderErrorLog(order, "IB OrderStatus: Inactive");
                        break;

                    case "None":
                        order.State = OrderStates.None;


                        break;

                    case "PartiallyFilled":
                        order.Balance = remaining;

                        break;
                    case "PendingCancel":

                        break;

                    case "PendingSubmit":
                        order.Status = OrderStatus.SentToServer;
                        break;
                    case "PreSubmitted":
                        order.Status = OrderStatus.SentToServer;
                        order.State = OrderStates.Active;
                        break;
                    case "Submitted":
                        order.Status = OrderStatus.Accepted;
                        order.Time = GetMarketTime(order.Security.ExchangeBoard.Exchange);
                        order.State = OrderStates.Active;

                        break;
                }
                order.LastChangeTime = GetMarketTime(order.Security.ExchangeBoard.Exchange);

                RaiseOrderChanged(order);
            }
        }

        private void OnUpdatePortfolio(IContract contract, int pos, double marketPrice, double marketValue,
                                       double averageCost, double unrealizedPNL, double realizedPNL, string accountName)
        {
            Portfolio portfolio = GetPortfolio(accountName);
            Security security = GetSecurity(contract);

            Position position = GetPosition(portfolio, security);

            var currentPrice = (decimal)marketPrice;
            var averagePrice = (decimal)averageCost;
            var realizedPnL = (decimal)realizedPNL;
            var unrealizedPnL = (decimal)unrealizedPNL;
            int currentValue = pos;
            if (position.CurrentPrice != currentPrice ||
                position.AveragePrice != averagePrice ||
                position.RealizedPnL != realizedPnL ||
                position.UnrealizedPnL != unrealizedPnL ||
                position.CurrentValue != currentValue
                )
            {
                position.CurrentPrice = currentPrice;
                position.AveragePrice = averagePrice;
                position.RealizedPnL = realizedPnL;
                position.UnrealizedPnL = unrealizedPnL;
                position.CurrentValue = currentValue;


                position.AddValue("marketValue", (decimal)marketValue);
                RaisePositionsChanged(new[] { position });
            }
        }

        private void OnNextValidId(int nextId)
        {
            _nextOrderId = nextId;
            ((IncrementTransactionIdGenerator)TransactionIdGenerator).CurrentTransactionId = nextId - 1;
        }

        private void OnTickPrice(int id, int tickType, double price, int canAutoExecute)
        {
            if (!_securityMarketDataDictionary.ContainsKey(id))
            {
                this.AddErrorLog("MarketDataRequestId не найден.  ReqId= {0}" .Put(id));
                _ib.cancelMktData(id);
                return;
            }
            Security security = _securityMarketDataDictionary[id];
            switch (tickType)
            {
                case 1:
                    if(security.BestBid==null) security.BestBid= new Quote();
                    security.BestBid.Price = (decimal)price;
                    RaiseSecurityChanged(security);


                    break;

                case 2:
                    if (security.BestAsk == null) security.BestAsk = new Quote();
                  
                    security.BestAsk.Price = (decimal)price;
                    RaiseSecurityChanged(security);


                    break;
                case 4:
                    if (security.LastTrade == null)
                        security.LastTrade = EntityFactory.CreateTrade(security, GetMarketTime(security.ExchangeBoard.Exchange).Ticks);
                    security.LastTrade.Price = (decimal)price;
                    security.LastTrade.Time = GetMarketTime(security.ExchangeBoard.Exchange);;
                    RaiseSecurityChanged(security);

                    break;

                case 6:
                    security.HighPrice = (decimal)price;
                    RaiseSecurityChanged(security);


                    break;

                case 7:
                    security.LowPrice = (decimal)price;
                    RaiseSecurityChanged(security);
                    break;
                case 14:
                    security.OpenPrice = (decimal)price;

                    RaiseSecurityChanged(security);
                    break;
            }
        }

        private void OnTickSize(int id, int tickType, int tickSize)
        {
            if (!_securityMarketDataDictionary.ContainsKey(id))
            {
                this.AddErrorLog("MarketDataRequestId не найден.  ReqId= {0}" .Put(id));
                _ib.cancelMktData(id);
                return;
            }
            Security security = _securityMarketDataDictionary[id];
            switch (tickType)
            {
                case 0:
                    if (security.BestBid == null) security.BestBid = new Quote();
                    security.BestBid.Volume = tickSize;
                    RaiseSecurityChanged(security);

                    break;
                case 3:
                    if (security.BestAsk == null) security.BestAsk = new Quote();
                    security.BestAsk.Volume = tickSize;
                    RaiseSecurityChanged(security);

                    break;


                case 5:
                    if (security.LastTrade == null)
                        security.LastTrade = EntityFactory.CreateTrade(security, GetMarketTime(security.ExchangeBoard.Exchange).Ticks);

                    security.LastTrade.Volume = tickSize;
                    //  if (security.LastTrade.Price > 0 && security.LastTrade.Volume > 0)
                    //      RaiseNewTrade(security.LastTrade);

                    RaiseSecurityChanged(security);
                    break;
            }
        }

        private void OnTickSnapshotEnd(int reqId)
        {
            if (_securityMarketDataDictionary.ContainsKey(reqId))
            {
                Security security = _securityMarketDataDictionary[reqId];

                this.AddDebugLog("TickSnapshotEnd: {0} " .Put(security.Code));
            }
        }


        private void OnContractDetails(int reqId, IContractDetails contractDetails)
        {
            Security security = _securityDetailsDictionary[reqId];
            security.Name = contractDetails.longName;
        }

        private void OnOpenOrder(int orderId, IContract contract, IOrder order, IOrderState orderState)
        {
            this.AddDebugLog("OnOpenOrder. orderId= {0} secCode= {1}" .Put( orderId,contract.symbol));

           

            Security security = GetSecurity(contract);
            GetOrder(order, security, orderState);
        }

        private void OnExecDetails(int reqId, IContract contract, IExecution execution)
        {
            this.AddDebugLog("OnExecDet");

            int orderId = execution.orderId;


            string tradeId = execution.execId;
            DateTime time = DateTime.ParseExact(execution.time, "yyyyMMdd  HH:mm:ss",
                                                CultureInfo.InvariantCulture);
            Portfolio portfolio = GetPortfolio(execution.acctNumber);


            OrderDirections? orderDir = null;
            if (execution.side.Equals("BOT")) orderDir = OrderDirections.Buy;
            else if (execution.side.Equals("SLD")) orderDir = OrderDirections.Sell;


            int volume = execution.shares;
            var price = (decimal)execution.price;
            int permId = execution.permId;
            int clientId = execution.clientId;
            int liquidation = execution.liquidation;
            int cumulativeQuantity = execution.cumQty;
            double averagePrice = execution.avgPrice;
            string orderRef = execution.orderRef;
            string evRule = execution.evRule;
            double evMultiplier = execution.evMultiplier;

            if (orderId == 0)
                return;

            Security security = GetSecurity(contract);


            Order order = GetOrderByTransactionId(orderId);

            if (order == null)
            {
                Order readOrder = _entityRegistry.Orders.ReadById(orderId);

                if (readOrder != null)
                {
                    order = EntityFactory.CreateOrder(security, orderId);
                    order.TransactionId = orderId;
                    //

                    order = GetOrder(security, orderId, id =>
                    {
                        using (order.BeginUpdate())
                        {
                            order.Id = id;
                            order.Direction = readOrder.Direction;
                            order.Type = readOrder.Type;
                            order.Volume = readOrder.Volume;
                            order.Price = readOrder.Price;
                            order.ExpiryDate = readOrder.ExpiryDate;
                            order.Portfolio = portfolio;
                            order.State = readOrder.State;
                            order.Status = readOrder.Status;
                            order.Time = readOrder.Time;
                            order.LatencyRegistration =
                                readOrder.LatencyRegistration;
                            order.LatencyCancellation =
                                readOrder.LatencyCancellation;
                            order.LastChangeTime = readOrder.LastChangeTime;
                        }

                        return order;
                    }, o => false);


                  //  RaiseNewOrder(order);
                }
            }

            AddMyTrade(security, orderId, 0, tradeId.GetHashCode(), id =>
            {
                Trade trade =
                    EntityFactory.CreateTrade(security, id);

                trade.Time = time;

                trade.OrderDirection = orderDir;
                trade.Volume = volume;
                trade.Price = price;
               

                return trade;
            }, mt =>
            {
                if (permId != null)
                    mt.AddValue("permId", permId);

                if (clientId != null)
                    mt.AddValue("clientId", clientId);

                if (liquidation != null)
                    mt.AddValue("liquidation",
                                liquidation);

                if (cumulativeQuantity != null)
                    mt.AddValue(
                        "cumulativeQuantity",
                        cumulativeQuantity);

                if (averagePrice != null)
                    mt.AddValue("averagePrice",
                                averagePrice);

                if (orderRef != null)
                    mt.AddValue("orderRef", orderRef);

                if (evRule != null)
                    mt.AddValue("evRule", evRule);

                if (evMultiplier != null)
                    mt.AddValue("evMultiplier",
                                evMultiplier);

                /*  _tradesByIds.Add(tradeId, mt);

var commissions = _tradesCommissions.TryGetValue(tradeId);
if (commissions != null)
{
_tradesCommissions.Remove(tradeId);
commissions.ForEach(NewCommission.SafeInvoke);
}*/
            });
        }

        private void OnConnectionClosed()
        {
            IsConnected = false;
            RaiseDisconnected();
        }

        private void OnAccountDownloadEnd(string accountName)
        {
            Portfolio portfolio = GetPortfolio(accountName);

            portfolio.Currency = CurrencyTypes.USD;

            portfolio.BeginValue = decimal.Parse(portfolio.GetValue<string>("PreviousDayEquityWithLoanValue"),
                                                 CultureInfo.InvariantCulture);

            portfolio.RealizedPnL = decimal.Parse(portfolio.GetValue<string>("RealizedPnL"),
                                                  CultureInfo.InvariantCulture);
            portfolio.UnrealizedPnL = decimal.Parse(portfolio.GetValue<string>("UnrealizedPnL"),
                                                    CultureInfo.InvariantCulture);


            portfolio.CurrentValue = decimal.Parse(portfolio.GetValue<string>("EquityWithLoanValue"),
                                                   CultureInfo.InvariantCulture);

            portfolio.BlockedValue = decimal.Parse(portfolio.GetValue<string>("FullInitMarginReq"),
                                                   CultureInfo.InvariantCulture);
            switch (portfolio.GetValue<string>("Currency"))
            {
                case "USD":
                    portfolio.Currency = CurrencyTypes.USD;
                    break;


                default:
                    portfolio.Currency = CurrencyTypes.USD;
                    break;
            }

            RaisePortfoliosChanged(new[] { portfolio });
        }

        /*
        public  void Save(EntityRegistry storage)
        {
            foreach (var position in Positions)
            {
                storage.Positions.Save(position);
            }

            foreach (var trade in MyTrades)
            {
                storage.MyTrades.Save(trade);
            }

            foreach (var order in Orders)
            {
                storage.Orders.Save(order);
            }

            foreach (var order in storage.Orders)
            {
                storage.Orders.Save(order);
            }
        }
         */

        protected override void DisposeManaged()
        {

            _ib.nextValidId -= OnNextValidId;
            _ib.connectionClosed -= OnConnectionClosed;
            _ib.updateAccountValue -= OnUpdateAccountValue;
            _ib.updatePortfolioEx -= OnUpdatePortfolio;
            _ib.tickPrice -= OnTickPrice;
            _ib.tickSize -= OnTickSize;
            _ib.tickSnapshotEnd -= OnTickSnapshotEnd;
            _ib.openOrderEx -= OnOpenOrder;
            _ib.orderStatus -= OnOrderStatus;
            _ib.contractDetailsEx -= OnContractDetails;

            _ib.accountDownloadEnd -= OnAccountDownloadEnd;
            _ib.errMsg -= OnError;
            _ib.execDetailsEx -= OnExecDetails;
            _ib.currentTime -= OnCurrentTime;
            _ib.realtimeBar -= OnRealTimeBar;
            _ib.historicalData -= OnHistoricalData;

            

            
            base.DisposeManaged();
        }


    }
}